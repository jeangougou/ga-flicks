// namespace
var gaflick;
// options container
var opts;

// construct application (check how it's called and enjoy the potential double undefined overwrite)
var gaflick = (function(gaflick, opts, $w, $u, undefined){
	
	// default initialization options
	var o = opts || {
		herokuUrl : 'https://dry-refuge-1252.herokuapp.com', // 'http://localhost:3000',
		searchBarId : 'flick-search',
		searchFormId : 'search-form',
		flickListMain : document.getElementById('flick-list'),
		favListMain : document.getElementById('favorites-list'),
	};
	
	// IE console fallback
	if('undefined' === typeof console || 'undefined' === typeof console.log ){
		console = {};
		console.log = console.error = console.warn = console.info = function(){};
	}
	
	// initial checks
	if(undefined !== $u)
	{
		throw "undefine overwritten: why ? why do you hate me so much ?";
	}
	
	// not really using it, but providing easy way to identify if loaded or not
	if(undefined == $w.jQuery)
	{
		// decent enough fallback if jquery is missing
		$ = function(q){return $w.document.querySelectorAll(q);};
	}else{
		console.warn("you might be using jquery and it's supposed to be vanilla javascript");
	}
	
	$w.onload = function () {
		listFavs();

		var searchForm = document.getElementById(o.searchFormId);
		var searchBar = document.getElementById(o.searchBarId);
		addEvent(searchForm,'submit', function (event) {
			event.preventDefault();
			searchFlick(searchBar.value);
		})
	};
	
	// initialization functions are a bit of a grey area, remember to make its behavior explicit
	var init = function(){
		console.info('initializing a single page application');
		console.info(arguments);
	};
	
	/*
	attach event wrapper
	el : element
	ev : event type
	fn : function, triggers on [ev] from [el]
	*/
	var addEvent = function(el,ev,fn){
		if(el.addEventListener){
			el.addEventListener( ev,fn, false );
		}else if(el.attachEvent){
			el.attachEvent( 'on'+ ev, fn );
		} else{
			el['on' + ev] = fn;
		}
	};
	
	/*
	remove event
	el : element
	ev : event type
	fn : function, triggers on [ev] from [el]
	*/
	var removeEvent = function(el,ev,fn){
		if(el.removeEventListener){
			el.removeEventListener( ev,fn, false );
		}else if(el.detachEvent){
			el.detachEvent( 'on'+ ev, fn );
		} else{
			el['on' + ev] = undefined;
		}
	}
	
	/*
	method : GET || POST
	url: heroku url
	callback: function (XMLHttpRequest, responseText)
	error: function (XMLHttpRequest, catched exception)
	*/
	var ajax = function(method, url, body, callback, error){
		var xmlHttpRequest = new XMLHttpRequest();
		xmlHttpRequest.onreadystatechange = function() {
			if (xmlHttpRequest.readyState==4 && xmlHttpRequest.status==200){
				if("function" === typeof callback){
					console.log(xmlHttpRequest.responseText);
					callback(xmlHttpRequest, xmlHttpRequest.responseText);
				}
			}
		};
		try{
			xmlHttpRequest.open(method, url, true);
			if('GET' === method)
			{
				xmlHttpRequest.send();
			} else if ('POST' === method) {
				xmlHttpRequest.setRequestHeader('Content-Type', 'application/json');
				console.info(body);
				xmlHttpRequest.send(body);
			}
		}catch(e){
			console.error(e);
			if("function" === typeof error){
					error.apply(xmlHttpRequest, e);
				}
			return;
		}
	};
	
	/*
	toSearch: what was typed in the search bar
	*/
	var searchFlick = function(toSearch){
		return ajax(
			'GET', 
			'https://www.omdbapi.com/?type=movie&r=json&y&s=' + toSearch,
			null,
			function(ajaxRequest, responseText){
				var allFlicksFound = JSON.parse(responseText).Search;
				for(var i = 0;  i < allFlicksFound.length; i++) {
					tplDisplayFlickInfo(allFlicksFound[i]);
				}
			},
		  function(ajaxRequest, exception){console.error(exception);}
		);
	};
	
	var evtLstFlickDetail = function(event){
		var self = event.target;
		event.preventDefault();
		if(self.parentElement.getAttribute('class').indexOf('selected') < 0){
  		self.parentElement.setAttribute('class', self.parentElement.getAttribute('class') + ' selected');
			getFlickDetail(self, self.innerHTML);
			removeEvent(self.parentElement, 'click', evtLstFlickDetail);
		}
	};
	
	var getFlickDetail = function(rootElement, title){
		return ajax(
			'GET', 
			'https://www.omdbapi.com/?y=&plot&r=json&t=' + title,
			null,
			function(ajaxRequest, responseText){
				var flickDetailItem = JSON.parse(responseText);
				tplDisplayFlickInfoDetail(rootElement, flickDetailItem);
			},
		  function(ajaxRequest, exception){console.error(exception);}
		);
	};
	
	// template function, easier to move to a view because we are already passing the model
	var tplDisplayFlickInfo = function(flickInfo){
		var flickInfoHtml = document.createElement('div');
		flickInfoHtml.setAttribute('class', 'flick-item');
		var flickTitle = document.createElement('div');
		flickTitle.setAttribute('class', 'flick-title');
		flickTitle.innerHTML = flickInfo.Title;
		addEvent(flickInfoHtml, 'click', evtLstFlickDetail);
		flickInfoHtml.appendChild(flickTitle);
		o.flickListMain.appendChild(flickInfoHtml);
	};
	
	
	// template for flick detail
	/*
	sample: {"Title":"Start the Revolution Without Me","Year":"1970","Rated":"M","Released":"14 Aug 1970","Runtime":"90 min","Genre":"Comedy, History","Director":"Bud Yorkin","Writer":"Fred Freeman, Lawrence J. Cohen","Actors":"Gene Wilder, Donald Sutherland, Hugh Griffith, Jack MacGowran","Plot":"Two mismatched sets of identical twins - one aristocrat, one peasant - mistakenly exchange identities on the eve of the French Revolution.","Language":"English","Country":"USA","Awards":"1 nomination.","Poster":"http://ia.media-imdb.com/images/M/MV5BMjM0MTk1NTgyOF5BMl5BanBnXkFtZTYwNTYyNzk5._V1_SX300.jpg","Metascore":"N/A","imdbRating":"6.6","imdbVotes":"1909","imdbID":"tt0066402","Type":"movie","Response":"True"}
	*/
	var tplDisplayFlickInfoDetail = function(rootElement, flickInfoDetail){
		// construct everything
		var flickInfoDetailHtml = document.createElement('div');
    flickInfoDetailHtml.setAttribute('class', 'flick-detail');
		// using microformat for flick detail (http://microformats.org/wiki/hmedia)
		
		var innerContent =flickInfoDetail.Year + ' - ' + flickInfoDetail.Title;
		if('N/A' != flickInfoDetail.Poster){
			innerContent = '<img class="photo" alt="'+ flickInfoDetail.Year + ' - ' + flickInfoDetail.Title + '" src="' + flickInfoDetail.Poster + '"/>';
		}

		var hReview = '<div class="hreview">	\
 <div class="item hmedia">	\
	<span>'+ flickInfoDetail.Year + ' - ' + flickInfoDetail.Title + '</span> \
  <a rel="enclosure" type="image/jpeg" href="http://www.imdb.com/title/'+flickInfoDetail.imdbID+'/">'+innerContent+'</a> \
    <p> \
      by: <span class="contributor vcard">	\
          <span class="fn director">Director/s '+flickInfoDetail.Director+ '</span><br>	\
					<span class="fn writer">Writer/s '+flickInfoDetail.Writer+ '</span><br>	\
					<span class="fn actors">Actors '+flickInfoDetail.Actors+ '</span><br>	\
     </span></p>	\
 </div>	\
 <div>Rating: <abbr class="rating" title="' + flickInfoDetail.imdbRating +'">' + flickInfoDetail.imdbRating +'/10</abbr></div>	\
  <div class="description">	\
    <p>' + flickInfoDetail.Plot +'</p> 	\
  </div>	\
 </div>';
    flickInfoDetailHtml.innerHTML = hReview;
		
		var favButton = document.createElement('a');
		favButton.setAttribute('class', 'fav-button');
		favButton.setAttribute('data-oid', flickInfoDetail.imdbID);
		favButton.setAttribute('data-name', flickInfoDetail.Title);
		removeEvent(favButton, 'click', evtLstFlickDetail);
		addEvent(favButton, 'click', eventListenerAddToFavs);
		favButton.innerHTML = 'Mark as favorite:' + flickInfoDetail.Title;
		flickInfoDetailHtml.appendChild(favButton);
		
		// single dom manipulation per call, dom manipulation is expensive, how can we make it better ?
		rootElement.appendChild(flickInfoDetailHtml);
	};
	
	var eventListenerAddToFavs = function(event){
		var self = event.target;
		event.preventDefault();
		saveFavs(self.getAttribute('data-oid'), self.getAttribute('data-name'));
		removeEvent(self, 'click', eventListenerAddToFavs);		
	};
		
	var saveFavs = function(oid, name){
		return ajax(
			'POST', 
			o.herokuUrl + '/favorites',
			JSON.stringify({"oid":oid, "name":name}),
			function(ajaxRequest, responseText){
				listFavs();
			},
			function(ajaxRequest, exception){console.error(exception);}
		);
	};
	
	// single favorite element template
	var tplDisplayFavs = function(favItem){
		var favToAppend = document.createElement('div');
		favToAppend.innerHTML = favItem.name;
		o.favListMain.appendChild(favToAppend);
	};
	
	// list all saved favorites from the server
	var listFavs = function(){
		return ajax(
			'GET', 
			o.herokuUrl + '/favorites',
			null,
			function(ajaxRequest, responseText){
				var favList = JSON.parse(responseText);
				o.favListMain.innerHTML = '';
				for (var i = 0; i < favList.length; i++){
					tplDisplayFavs(favList[i]);
				}
			},
			function(ajaxRequest, exception){console.error(exception);}
		);
	};
		
	// assign and choose what to expose
	gaflick = {
		init : init,
		ajax : ajax,
		//// hidden from the outside
		// addEvent : addEvent,  
		// removeEvent : removeEvent,
		
		// change name
		search: searchFlick,
		displayFlickInfo: tplDisplayFlickInfo, 
		displayFlickInfoDetail : tplDisplayFlickInfoDetail,
		getFlick : getFlickDetail,
		
		listFavs : listFavs,
		tplDisplayFavs : tplDisplayFavs,
		saveFavs: saveFavs,
		
		// debug method
		debug : function (){
			return o;
		}
	};
	
	// that's all nice and wrapped up :)
	return gaflick;
	// IIF: use namespace or assign empty object, options on initialization, window, jQuery if present for library identification, undefined reference
})(gaflick || (gaflick = {}), opts, window, undefined);